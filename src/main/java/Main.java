import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersRequest;
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersResult;
import com.amazonaws.services.simplesystemsmanagement.model.ParameterMetadata;
import com.amazonaws.services.simplesystemsmanagement.model.ParametersFilter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

  public static void main(String[] args) {
    String prefix = args.length > 1 ? args[0] : "vyom_backend_dev_payments_";
    ParametersFilter filter =
        new ParametersFilter().withKey("Name").withValues(prefix); // the key prefix i want

    DescribeParametersRequest request = new DescribeParametersRequest().withFilters(filter);

    AWSSimpleSystemsManagement client = AWSSimpleSystemsManagementClientBuilder.defaultClient();

    Set<String> paramNames = new HashSet<>();

    DescribeParametersResult result = client.describeParameters(request);
    String nextToken = result.getNextToken();

    while (nextToken != null && nextToken.length() > 0) {
      List<ParameterMetadata> params = result.getParameters();
      for (ParameterMetadata param : params) {
        String paramName = param.getName();
        paramNames.add(paramName);
        System.out.println(paramName);
      }

      result = client.describeParameters(request.withNextToken(nextToken));
      nextToken = result.getNextToken();
    }
    //        params.stream().map(ParameterMetadata::getName).collect(Collectors.toSet());
  }
}
